/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        'trans-blue': '#55cdfc',
        'trans-pink': '#f7a8b8',
        'bi-red': '#d60270',
        'bi-violet': '#9b4f96',
        'bi-blue': '#0038a8'
      },
      fontFamily: {
        jetbrains: ["JetBrains Mono", "sans-serif"],
      },
      maxWidth: {
        'vw': '100vw'
      },
      spacing: {
        none: '0',
        xs: '0.25em',
        sm: '0.5em',
        small: '0.5em',
        md: '0.75em',
        lg: '1em',
        base: '1em',
        big: '1.5em',
        xl: '1.25em',
      },

    },
  },
  plugins: [],
}